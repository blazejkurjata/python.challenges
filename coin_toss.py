import random

def coin_toss():
    """Randomly return 'heads' or 'tails'."""
    if random.randint(0, 1) == 0:
        return "heads"
    else:
        return "tails"


heads = 0
tails = 0
counter = 0
toss_num = 10_000

for trail in range (toss_num):
    while ((heads == 0) or (tails == 0)):
        counter = counter + 1
        if coin_toss() == "heads":
            heads = heads + 1
        else:
            tails = tails + 1
    heads = 0
    tails = 0

print(f"The average number of flips to contain both heads and tails is {counter/toss_num}")
