number = int(input("Enter a positive integer: "))

for factor in range(1, number + 1):
    if number % factor == 0:
        print(f"{factor} is a factor of {number}")

