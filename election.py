"""
Suppose two candidates, Candidate A and Candidate B, are running
for mayor in a city with three voting regions. The most recent polls
show that Candidate A has the following chances for winning in each
region:
• Region 1: 87 percent chance of winning
• Region 2: 65 percent chance of winning
• Region 3: 17 percent chance of winning
Write a program that simulates the election ten thousand times and
prints the percentage of times in which Candidate A wins.
To keep things simple, assume that a candidate wins the election if
they win in at least two of the three regions.
"""

import random

def election(A_chances):
    
    """Return A or B taking into account chances in regions"""
    
    if random.random() < A_chances:
        return "A"
    else:
        return "B"

def run_election(regional_chances):
    
    """Return numbers of wins by candidates A and B"""
    
    A_wins = 0
    B_wins = 0
    for A_chances in regional_chances:
        if election(A_chances) == "A":
            A_wins += 1
        else:
            B_wins += 1
    if A_wins >= 2:
        return "A"
    else:
        return "B"
    A_wins = 0
    B_wins = 0    

A_chances_by_region = [0.87, 0.65, 0.17]
trails = 10_000
A_election_win = 0
B_election_win = 0
for trial in range(trails):
    if run_election(A_chances_by_region) == "A":
        A_election_win += 1
    else:
        B_election_win += 1

print(f"Candidate A won in {A_election_win/trails:.4f}% cases.")
print(f"Candidate B won in {B_election_win/trails:.4f}% cases.")
