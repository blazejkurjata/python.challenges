import random

capitals_dict = {
    "Alabama": "Montgomery",
    "Alaska": "Juneau",
    "Arizona": "Phoenix",
    "Arkansas": "Little Rock",
    "California": "Sacramento",
    "Colorado": "Denver",
    "Connecticut": "Hartford",
    "Delaware": "Dover",
    "Florida": "Tallahassee",
    "Georgia": "Atlanta",
    "Hawaii": "Honolulu",
    "Idaho": "Boise",
    "Illinois": "Springfield",
    "Indiana": "Indianapolis",
    "Iowa": "Des Moines",
    "Kansas": "Topeka",
    "Kentucky": "Frankfort",
    "Louisiana": "Baton Rouge",
    "Maine": "Augusta",
    "Maryland": "Annapolis",
    "Massachusetts": "Boston",
    "Michigan": "Lansing",
    "Minnesota": "Saint Paul",
    "Mississippi": "Jackson",
    "Missouri": "Jefferson City",
    "Montana": "Helena",
    "Nebraska": "Lincoln",
    "Nevada": "Carson City",
    "New Hampshire": "Concord",
    "New Jersey": "Trenton",
    "New Mexico": "Santa Fe",
    "New York": "Albany",
    "North Carolina": "Raleigh",
    "North Dakota": "Bismarck",
    "Ohio": "Columbus",
    "Oklahoma": "Oklahoma City",
    "Oregon": "Salem",
    "Pennsylvania": "Harrisburg",
    "Rhode Island": "Providence",
    "South Carolina": "Columbia",
    "South Dakota": "Pierre",
    "Tennessee": "Nashville",
    "Texas": "Austin",
    "Utah": "Salt Lake City",
    "Vermont": "Montpelier",
    "Virginia": "Richmond",
    "Washington": "Olympia",
    "West Virginia": "Charleston",
    "Wisconsin": "Madison",
    "Wyoming": "Cheyenne",
    }

state_chosen = ""
searched_capital = ""
state_list = []

for state in capitals_dict:
    state_list.append(state)

for state in state_list:
    state_chosen = random.choice(state_list)
    searched_capital = capitals_dict[state_chosen]

while True:
    user_answer = input("What is the capital of " + state_chosen + "? ").lower()
    if user_answer == "exit":
        print(f"The capital of {state_chosen} is {searched_capital}. Goodbye.")
        break
    elif user_answer == capitals_dict[state_chosen].lower():
        print("That's correct answer. Congratulations.")
        break

"""
Review your state capitals along with your knowledge of dictionaries
and while loops!
First, fill out the following dictionary with the remaining states and
their associated capitals in a file called capitals.py:

capitals_dict = {
'Alabama': 'Montgomery',
'Alaska': 'Juneau',
'Arizona': 'Phoenix',
'Arkansas': 'Little Rock',
'California': 'Sacramento',
'Colorado': 'Denver',
'Connecticut': 'Hartford',
'Delaware': 'Dover',
'Florida': 'Tallahassee',
'Georgia': 'Atlanta',
}

Next, pick a random state name from the dictionary and assign both
the state and its capital to two variables. You’ll need to import the
random module at the top of your program.

Then display the name of the state to the user and ask them to enter
the capital. If the user answers incorrectly, then repeatedly ask them
for the capital until they either enter the correct answer or type "exit".
If the user answers correctly, then display "Correct" and end the pro-
gram. However, if the user exits without guessing correctly, display
the correct answer and the word "Goodbye".

Note
Make sure the user isn’t punished for case sensitivity. In other
words, a guess of "Denver" is the same as "denver". Do the same
for exiting—"EXIT" and "Exit" should work the same as "exit".
"""
