import random

def choose_words(words_list, words_number):
    
    """Return list of random words chosen from given lists."""
    
    random_words = []
    random_word = 0

    while len(random_words) < words_number:
        random_word = random.choice(words_list)
        if random_word in random_words:
            continue
        else:
            random_words.append(random_word)
    return random_words


nouns = [
    "fossil",
    "horse",
    "aardvark",
    "judge",
    "chef",
    "mango",
    "extrovert",
    "gorilla"
]
verbs = [
    "kicks",
    "jingles",
    "bounces",
    "slurps",
    "meows",
    "explodes",
    "curdles"
]
adjectives = [
    "furry",
    "balding",
    "incredulous",
    "fragrant",
    "exuberant",
    "glistening"
]
prepositions = [
    "against",
    "after",
    "into",
    "beneath",
    "upon",
    "for",
    "in",
    "like",
    "over",
    "within"
]
adverbs = [
    "curiously",
    "extravagantly",
    "tantalizingly",
    "furiously",
    "sensuously"
]

def make_poem():

    """Create a randomly generated poem."""
    
    rdm_noun = choose_words(nouns, 3)
    rdm_verb = choose_words(verbs, 3)
    rdm_adj = choose_words(adjectives, 3)
    rdm_prep = choose_words(prepositions, 2)
    rdm_adv = choose_words(adverbs, 1)

    art1 = 0
    vowels = ["a", "e", "i", "o", "u"]
    if (rdm_adj[0][0].lower()) in vowels:
        art1 = "An"
    else:
        art1 = "A"

    art2 = 0
    vowels = ["a", "e", "i", "o", "u"]
    if (rdm_adj[2][0].lower()) in vowels:
        art2 = "an"
    else:
        art2 = "a"

    poem = (

    f"{art1} {rdm_adj[0]} {rdm_noun[0]}\n\n"
    f"{art1} {rdm_adj[0]} {rdm_noun[0]} {rdm_verb[0]} {rdm_prep[0]} the \
{rdm_adj[1]} {rdm_noun[1]}\n"
    f"{rdm_adv[0]}, the {rdm_noun[0]} {rdm_verb[1]}\n"
    f"the {rdm_noun[1]} {rdm_verb[2]} {rdm_prep[1]} {art2} {rdm_adj[2]} \
{rdm_noun[2]}"
    )

    return poem

poem = make_poem()
print(poem)


"""
Task:
In this challenge, you’ll write a program that generates poetry.
Create five lists for different word types:
1. Nouns: ["fossil", "horse", "aardvark", "judge", "chef", "mango",
"extrovert", "gorilla"]
2. Verbs:
["kicks","jingles","bounces","slurps","meows","explodes", "curdles"]
3. Adjectives:
["furry","balding","incredulous","fragrant","exuberant", "glistening"]
4. Prepositions: ["against","after","into","beneath","upon","for", "in", "like", "over", "within"]
5. Adverbs:
["curiously","extravagantly","tantalizingly","furiously", "sensuously"]
Randomly select the following number of elements from each list:
• Three nouns
• Three verbs
• Three adjectives
• Two prepositions
• One adverb
You can do this with the choice() function in the random module. This
function takes a list as input and returns a randomly selected element
of the list.
For example, here’s how you use random.choice() to get random element
from the list ["a", "b", "c"]:
import random
random_element = random.choice(["a", "b", "c"])
Using the randomly selected words, generate and display a poem with
the following structure inspired by Clifford Pickover:

{A/An} {adj1} {noun1}
{A/An} {adj1} {noun1} {verb1} {prep1} the {adj2} {noun2}
{adverb1}, the {noun1} {verb2}
the {noun2} {verb3} {prep2} a {adj3} {noun3}
Here, adj stands for adjective and prep for preposition.

Here’s an example of the kind of poem your program might generate:

A furry horse
A furry horse curdles within the fragrant mango
extravagantly, the horse slurps
the mango meows beneath a balding extrovert

Each time your program runs, it should generate a new poem.
"""
