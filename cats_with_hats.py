cats_dict = {}

for cat in range(1, 101):
    cats_dict[chr(35) + str(cat)] = False

k = 1
while k < 101:
    for i in range(k - 1, len(cats_dict), k):
        cats_dict[chr(35) + str(i + 1)] = not(cats_dict[chr(35) + str(i + 1)])
    k += 1

for cats, hats in cats_dict.items():
    if hats == True:
        print(f"Cat {cats} has a hat")

"""
You have one hundred cats.
One day, you decide to arrange all your cats in a giant circle. Initially,
none of your cats has a hat on. You walk around the circle a hundred
times, always starting with the first cat (cat #1). Each time you stop at
a cat, you check if it has a hat on. If it doesn’t, then you put a hat on
it. If it does, then you take the hat off.
1. The first round, you stop at every cat, placing a hat on each one.
2. The second round, you stop only at every second cat (#2, #4, #6,
#8, and so on).
3. The third round, you stop only at every third cat (#3, #6, #9, #12,
and so on).
4. You continue this process until you’ve made one hundred rounds
around the cats. On the last round, you stop only at cat #100.
Write a program that simply outputs which cats have hats at the end.
"""
