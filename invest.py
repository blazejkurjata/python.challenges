def invest(amount, rate, years):
    """Calculate growing amount of investment based on principal amount, annual
    rate of return and number of years of investment"""
    for year in range(1, years + 1):
        amount = amount * (rate + 1)
        print(f"year {year}: ${amount:,.2f}")
    

amount = float(input("Enter principal amount: "))
rate = float(input("Enter annual rate of return: "))
years = int(input("Number of years: "))

invest(amount, rate, years)
