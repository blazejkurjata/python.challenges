def enrollment_stats(university):
    
    """
    Return 2 lists:
    first containing total number of enrolled students,
    second containing annual tuition fees.
    """
    
    enrolled_students = []
    tuition_fees = []
    j = 0
    for i in university:
        enrolled_students.append(university[j][1])
        tuition_fees.append(university[j][2])
        j += 1
    return enrolled_students, tuition_fees


def mean(source):

    """Return mean of given values."""
    
    return sum(source) / len(source)


def median(source):

    """Return median of given values."""
    
    source.sort()
    if len(source) % 2 == 1:
        middle_index = int(len(source) / 2)
        return source[middle_index]
    else:
        left_middle_index = (len(source) - 1) / 2
        right_middle_index = (len(source) + 1) / 2
        return mean([left_middle_index, right_middle_index])


universities = [
    ["California Institute of Technology", 2175, 37704],
    ["Harvard", 19627, 39849],
    ["Massachusetts Institute of Technology", 10566, 40732],
    ["Princeton", 7802, 37000],
    ["Rice", 5879, 35551],
    ["Stanford", 19535, 40569],
    ["Yale", 11701, 40500]
]

summary = enrollment_stats(universities)

print("*****" * 6)
print(f"Total students:    {sum(summary[0]):,}")
print(f"Total tuition    $ {sum(summary[1]):,}")
print(f"\nStudent mean:      {mean(summary[0]):,.2f}")
print(f"Student median:    {median(summary[0]):,}")
print(f"\nTuition mean:    $ {mean(summary[1]):,.2f}")
print(f"Tuition median:  $ {median(summary[1]):,}")
print("*****" * 6)

"""
Task:

Write a program that contains the following lists of lists:
universities = [
['California Institute of Technology', 2175, 37704],
['Harvard', 19627, 39849],
['Massachusetts Institute of Technology', 10566, 40732],
['Princeton', 7802, 37000],
['Rice', 5879, 35551],
['Stanford', 19535, 40569],
['Yale', 11701, 40500]
]

Define a function, enrollment_stats(), with a single parameter. This
parameter should be a list of lists in which each individual list contains
three elements:
1. The name of a university
2. The total number of enrolled students
3. The annual tuition fees

enrollment_stats() should return two lists, the first containing all the
student enrollment values and the second containing all the tuition
fees.

Next, define two functions, mean() and median(), that take a single list
argument and return the mean or median of the values in each list,
respectively.

Using universities, enrollment_stats(), mean(), and median(), calculate
the total number of students, the total tuition, the mean and median
numbers of students, and the mean and median tuition values.
"""
